﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StudentCRUDEntity.Startup))]
namespace StudentCRUDEntity
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
