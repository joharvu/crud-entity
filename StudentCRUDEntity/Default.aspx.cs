﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StudentCRUDEntity
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UpdateStudent.Enabled = false;
        }

        protected void clear()
        {
            StuFname.Value = null;
            StuLname.Value = null;
            StuFatherName.Value = null;
            StuDob.Value = null;
            StuProgram.SelectedIndex = 0;
            StuCNIC.Value = null;
        }

        protected void AddStudent_Click(object sender, EventArgs e)
        {
            string stuProgramName = "";
            if (StuProgram.Value == "1")
            {
                stuProgramName = "Bachelors of Science BS";
            }
            else if (StuProgram.Value == "2")
            {
                stuProgramName = "Masters MS";
            }
            else if (StuProgram.Value == "3")
            {
                stuProgramName = "Medical MBBS";
            }

            if (StuFname.Value != "" && StuLname.Value != "" && StuFatherName.Value != "" && StuDob.Value != "" && StuProgram.Value != "0" && StuCNIC.Value != "" )
            {
                ErrorMessage.Text = "";

                DbStudentEntities dse = new DbStudentEntities();

                //object of student class to take input
                Student stu = new Student();

                stu.FirstName = StuFname.Value;
                stu.LastName = StuLname.Value;
                stu.FatherName = StuFatherName.Value;
                stu.DateOfBirth = Convert.ToDateTime(StuDob.Value);
                stu.Program = stuProgramName;
                stu.CNIC = StuCNIC.Value;

                //add entity to the add method
                dse.Students.Add(stu);

                dse.SaveChanges();
            
                GridView1.DataBind();
                clear();
            }
            else
            {
                ErrorMessage.Text = "Please fill the form";
            }

            

        }

        
       

        protected void deteleBtn_OnClick(object sender, EventArgs e)
        {


            int stuId = Convert.ToInt32(((sender as LinkButton).CommandArgument));
            

            DbStudentEntities dse = new DbStudentEntities();

            //object of student class to take input
            Student stu = new Student { ID = stuId };

            dse.Students.Attach(stu);
            dse.Students.Remove(stu);

            dse.SaveChanges();

            GridView1.DataBind();
        }

        protected void editBtn_OnClick(object sender, EventArgs e)
        {

            UpdateStudent.Enabled = true;
            AddStudent.Enabled = false;

            string stuProgramName = "";
            int stuProgramVal = 0;

            if (StuProgram.Value == "1")
            {
                stuProgramName = "Bachelors of Science BS";
            }
            else if (StuProgram.Value == "2")
            {
                stuProgramName = "Masters MS";
            }
            else if (StuProgram.Value == "3")
            {
                stuProgramName = "Medical MBBS";
            }

            
            


            int stuId = Convert.ToInt32(((sender as LinkButton).CommandArgument));

            DbStudentEntities dse = new DbStudentEntities();

            Student stu = dse.Students.Find(stuId);

            hiddenId.Value = stu.ID.ToString();
            StuFname.Value = stu.FirstName;
            StuLname.Value = stu.LastName;
            StuFatherName.Value = stu.FatherName;
            StuDob.Value = stu.DateOfBirth.ToString("d");

            if (stu.Program == "Bachelors of Science BS")
            {
                stuProgramVal = 1;
            }
            else if (stu.Program == "Masters MS")
            {
                stuProgramVal = 2;
            }
            else if (stu.Program == "Medical MBBS")
            {
                stuProgramVal = 1;
            }

            StuProgram.SelectedIndex = stuProgramVal;
            StuCNIC.Value = stu.CNIC;


        }

        protected void UpdateStudent_OnClick(object sender, EventArgs e)
        {

            AddStudent.Enabled = true;

            string stuProgramName = "";
            if (StuProgram.Value == "1")
            {
                stuProgramName = "Bachelors of Science BS";
            }
            else if (StuProgram.Value == "2")
            {
                stuProgramName = "Masters MS";
            }
            else if (StuProgram.Value == "3")
            {
                stuProgramName = "Medical MBBS";
            }

            try
            {

                DbStudentEntities dse = new DbStudentEntities();

                Student stu = dse.Students.Find(Convert.ToInt32(hiddenId.Value));

                stu.FirstName = StuFname.Value;
                stu.LastName = StuLname.Value;
                stu.FatherName = StuFatherName.Value;
                stu.DateOfBirth = Convert.ToDateTime(StuDob.Value);
                stu.Program = stuProgramName;
                stu.CNIC = StuCNIC.Value;


                dse.SaveChanges();

                GridView1.DataBind();

                clear();

            }
            catch (Exception exception)
            {
                    Console.WriteLine(exception);
                    throw;
            }
        }
    }
}